#!/bin/bash

RANGO_IP="170.51.255.240/24"
INTD="eth10"


function start {
	#inserto el kernel module dummy
	sudo modprobe dummy
	
	# seteo la interfaz con un nombre 
	sudo ip link set name $INTD dev dummy0
	
	# configuro el rango de ip 
	sudo ip addr add $RANGO_IP brd + dev $INTD
	
	#levanto la interfaz
	sudo ifconfig $INTD up

}


function stop {
	sudo ifconfig $INTD down
	sudo ip addr del $RANGO_IP brd + dev $INTD
}


function check {
	#saco la ip de la interfaz
	TEST=`ifconfig |grep -A3 $INTD | awk -F : '/inet / {print $2}' | awk '{print $1}'`
	if [[ $TEST == "" ]]
	then
		echo "Ocurrio un error o la interfaz no fue iniciada"
				
	elif [[ $RANGO_IP =~  $TEST.* ]]
	then
		echo "Configuracion OK IP: $TEST, INTERFAZ: $INTD"
	else
		echo "Ocurrio un error"
	fi
	
}

#entra en la funcion llamada
$1
